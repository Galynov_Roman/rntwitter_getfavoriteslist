/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import twitter, {auth} from 'react-native-twitter';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

const tokens = {
  consumerKey: '57ltXUOqelnpdQShEUhxBq7mM',
  consumerSecret: 'wSUOaTiURBHyE38mRbHkH3xj8svhDEYAVx4pcFZcA4RNMEDiQG',
  accessToken: '798093877232156672-drKHoi8Mv7VRAP6YUzUNUOqIX9QxOwu',
  accessTokenSecret: 'kv5J1ZxpilD5TEKoEMtabvT2gmeUqZxIRNpmk6zzYH0Ee',
};

const { rest } = twitter(tokens);

type Props = {};
export default class App extends Component<Props> {

  getDataFromTwitterApi = () => {
    rest.get('https://api.twitter.com/1.1/favorites/list')
    .then((data) => console.warn('data>>', data))
    .catch((err) => console.warn('error>>', err));
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native!
        </Text>
        <Text style={styles.instructions}>
          To get started, edit App.js
        </Text>
        <Text style={styles.instructions}>
          {instructions}
        </Text>
        <Button
          onPress={() => this.getDataFromTwitterApi()}
          title="get favorites list of tweets"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
