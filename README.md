#  Example, how use react-native-twitter for Twitter API

# useful links
* [API reference index](https://developer.twitter.com/en/docs/api-reference-index)
* [Twitter app manager](https://apps.twitter.com/)

* [react-native-twitter/iOS Example](https://github.com/Piroro-hs/react-native-twitter/issues/5)
## :arrow_up: How to Setup

**Step 1:** git clone this repo:

**Step 2:** cd to the cloned repo:

**Step 3:** Install the Application with `yarn` or `npm i`


## :arrow_forward: How to Run App

1. cd to the repo
2. Run Build for either OS
  * for iOS
    * run `react-native run-ios`
  * for Android
    * Run Genymotion
    * run `react-native run-android`
